package com.example.tema1.grpc;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.4.0)",
    comments = "Source: HelloService.proto")
public final class MedicationPlanServiceGrpc {

  private MedicationPlanServiceGrpc() {}

  public static final String SERVICE_NAME = "com.example.tema1.grpc.MedicationPlanService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.example.tema1.grpc.MedicationPlanRequest,
      com.example.tema1.grpc.MedicationPlan> METHOD_GET_MEDICATION_PLAN =
      io.grpc.MethodDescriptor.<com.example.tema1.grpc.MedicationPlanRequest, com.example.tema1.grpc.MedicationPlan>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "com.example.tema1.grpc.MedicationPlanService", "getMedicationPlan"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.example.tema1.grpc.MedicationPlanRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.example.tema1.grpc.MedicationPlan.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.example.tema1.grpc.MedicationTaken,
      com.example.tema1.grpc.MedicationTakenResponse> METHOD_MEDICATION_TAKEN =
      io.grpc.MethodDescriptor.<com.example.tema1.grpc.MedicationTaken, com.example.tema1.grpc.MedicationTakenResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "com.example.tema1.grpc.MedicationPlanService", "medicationTaken"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.example.tema1.grpc.MedicationTaken.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.example.tema1.grpc.MedicationTakenResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.example.tema1.grpc.MedicationTaken,
      com.example.tema1.grpc.MedicationTakenResponse> METHOD_MEDICATION_NOT_TAKEN =
      io.grpc.MethodDescriptor.<com.example.tema1.grpc.MedicationTaken, com.example.tema1.grpc.MedicationTakenResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "com.example.tema1.grpc.MedicationPlanService", "medicationNotTaken"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.example.tema1.grpc.MedicationTaken.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.example.tema1.grpc.MedicationTakenResponse.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MedicationPlanServiceStub newStub(io.grpc.Channel channel) {
    return new MedicationPlanServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MedicationPlanServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new MedicationPlanServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MedicationPlanServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new MedicationPlanServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class MedicationPlanServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getMedicationPlan(com.example.tema1.grpc.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationPlan> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_MEDICATION_PLAN, responseObserver);
    }

    /**
     */
    public void medicationTaken(com.example.tema1.grpc.MedicationTaken request,
        io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationTakenResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_MEDICATION_TAKEN, responseObserver);
    }

    /**
     */
    public void medicationNotTaken(com.example.tema1.grpc.MedicationTaken request,
        io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationTakenResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_MEDICATION_NOT_TAKEN, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_GET_MEDICATION_PLAN,
            asyncUnaryCall(
              new MethodHandlers<
                com.example.tema1.grpc.MedicationPlanRequest,
                com.example.tema1.grpc.MedicationPlan>(
                  this, METHODID_GET_MEDICATION_PLAN)))
          .addMethod(
            METHOD_MEDICATION_TAKEN,
            asyncUnaryCall(
              new MethodHandlers<
                com.example.tema1.grpc.MedicationTaken,
                com.example.tema1.grpc.MedicationTakenResponse>(
                  this, METHODID_MEDICATION_TAKEN)))
          .addMethod(
            METHOD_MEDICATION_NOT_TAKEN,
            asyncUnaryCall(
              new MethodHandlers<
                com.example.tema1.grpc.MedicationTaken,
                com.example.tema1.grpc.MedicationTakenResponse>(
                  this, METHODID_MEDICATION_NOT_TAKEN)))
          .build();
    }
  }

  /**
   */
  public static final class MedicationPlanServiceStub extends io.grpc.stub.AbstractStub<MedicationPlanServiceStub> {
    private MedicationPlanServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicationPlanServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicationPlanServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicationPlanServiceStub(channel, callOptions);
    }

    /**
     */
    public void getMedicationPlan(com.example.tema1.grpc.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationPlan> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET_MEDICATION_PLAN, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void medicationTaken(com.example.tema1.grpc.MedicationTaken request,
        io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationTakenResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_MEDICATION_TAKEN, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void medicationNotTaken(com.example.tema1.grpc.MedicationTaken request,
        io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationTakenResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_MEDICATION_NOT_TAKEN, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MedicationPlanServiceBlockingStub extends io.grpc.stub.AbstractStub<MedicationPlanServiceBlockingStub> {
    private MedicationPlanServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicationPlanServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicationPlanServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicationPlanServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.example.tema1.grpc.MedicationPlan getMedicationPlan(com.example.tema1.grpc.MedicationPlanRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_GET_MEDICATION_PLAN, getCallOptions(), request);
    }

    /**
     */
    public com.example.tema1.grpc.MedicationTakenResponse medicationTaken(com.example.tema1.grpc.MedicationTaken request) {
      return blockingUnaryCall(
          getChannel(), METHOD_MEDICATION_TAKEN, getCallOptions(), request);
    }

    /**
     */
    public com.example.tema1.grpc.MedicationTakenResponse medicationNotTaken(com.example.tema1.grpc.MedicationTaken request) {
      return blockingUnaryCall(
          getChannel(), METHOD_MEDICATION_NOT_TAKEN, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MedicationPlanServiceFutureStub extends io.grpc.stub.AbstractStub<MedicationPlanServiceFutureStub> {
    private MedicationPlanServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicationPlanServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicationPlanServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicationPlanServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.tema1.grpc.MedicationPlan> getMedicationPlan(
        com.example.tema1.grpc.MedicationPlanRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET_MEDICATION_PLAN, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.tema1.grpc.MedicationTakenResponse> medicationTaken(
        com.example.tema1.grpc.MedicationTaken request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_MEDICATION_TAKEN, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.tema1.grpc.MedicationTakenResponse> medicationNotTaken(
        com.example.tema1.grpc.MedicationTaken request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_MEDICATION_NOT_TAKEN, getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_MEDICATION_PLAN = 0;
  private static final int METHODID_MEDICATION_TAKEN = 1;
  private static final int METHODID_MEDICATION_NOT_TAKEN = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MedicationPlanServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MedicationPlanServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_MEDICATION_PLAN:
          serviceImpl.getMedicationPlan((com.example.tema1.grpc.MedicationPlanRequest) request,
              (io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationPlan>) responseObserver);
          break;
        case METHODID_MEDICATION_TAKEN:
          serviceImpl.medicationTaken((com.example.tema1.grpc.MedicationTaken) request,
              (io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationTakenResponse>) responseObserver);
          break;
        case METHODID_MEDICATION_NOT_TAKEN:
          serviceImpl.medicationNotTaken((com.example.tema1.grpc.MedicationTaken) request,
              (io.grpc.stub.StreamObserver<com.example.tema1.grpc.MedicationTakenResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class MedicationPlanServiceDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.example.tema1.grpc.HelloService.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MedicationPlanServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MedicationPlanServiceDescriptorSupplier())
              .addMethod(METHOD_GET_MEDICATION_PLAN)
              .addMethod(METHOD_MEDICATION_TAKEN)
              .addMethod(METHOD_MEDICATION_NOT_TAKEN)
              .build();
        }
      }
    }
    return result;
  }
}
