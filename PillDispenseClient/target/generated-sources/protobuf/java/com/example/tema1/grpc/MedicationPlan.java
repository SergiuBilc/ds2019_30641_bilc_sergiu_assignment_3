// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: HelloService.proto

package com.example.tema1.grpc;

/**
 * Protobuf type {@code com.example.tema1.grpc.MedicationPlan}
 */
public  final class MedicationPlan extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:com.example.tema1.grpc.MedicationPlan)
    MedicationPlanOrBuilder {
  // Use MedicationPlan.newBuilder() to construct.
  private MedicationPlan(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private MedicationPlan() {
    medicationPlan_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private MedicationPlan(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            if (!((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
              medicationPlan_ = new java.util.ArrayList<com.example.tema1.grpc.IntakeGrpc>();
              mutable_bitField0_ |= 0x00000001;
            }
            medicationPlan_.add(
                input.readMessage(com.example.tema1.grpc.IntakeGrpc.parser(), extensionRegistry));
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
        medicationPlan_ = java.util.Collections.unmodifiableList(medicationPlan_);
      }
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationPlan_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationPlan_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.example.tema1.grpc.MedicationPlan.class, com.example.tema1.grpc.MedicationPlan.Builder.class);
  }

  public static final int MEDICATIONPLAN_FIELD_NUMBER = 1;
  private java.util.List<com.example.tema1.grpc.IntakeGrpc> medicationPlan_;
  /**
   * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
   */
  public java.util.List<com.example.tema1.grpc.IntakeGrpc> getMedicationPlanList() {
    return medicationPlan_;
  }
  /**
   * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
   */
  public java.util.List<? extends com.example.tema1.grpc.IntakeGrpcOrBuilder> 
      getMedicationPlanOrBuilderList() {
    return medicationPlan_;
  }
  /**
   * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
   */
  public int getMedicationPlanCount() {
    return medicationPlan_.size();
  }
  /**
   * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
   */
  public com.example.tema1.grpc.IntakeGrpc getMedicationPlan(int index) {
    return medicationPlan_.get(index);
  }
  /**
   * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
   */
  public com.example.tema1.grpc.IntakeGrpcOrBuilder getMedicationPlanOrBuilder(
      int index) {
    return medicationPlan_.get(index);
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (int i = 0; i < medicationPlan_.size(); i++) {
      output.writeMessage(1, medicationPlan_.get(i));
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    for (int i = 0; i < medicationPlan_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, medicationPlan_.get(i));
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.example.tema1.grpc.MedicationPlan)) {
      return super.equals(obj);
    }
    com.example.tema1.grpc.MedicationPlan other = (com.example.tema1.grpc.MedicationPlan) obj;

    boolean result = true;
    result = result && getMedicationPlanList()
        .equals(other.getMedicationPlanList());
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (getMedicationPlanCount() > 0) {
      hash = (37 * hash) + MEDICATIONPLAN_FIELD_NUMBER;
      hash = (53 * hash) + getMedicationPlanList().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.example.tema1.grpc.MedicationPlan parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationPlan parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.example.tema1.grpc.MedicationPlan parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.example.tema1.grpc.MedicationPlan parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.example.tema1.grpc.MedicationPlan prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.example.tema1.grpc.MedicationPlan}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.example.tema1.grpc.MedicationPlan)
      com.example.tema1.grpc.MedicationPlanOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationPlan_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationPlan_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.example.tema1.grpc.MedicationPlan.class, com.example.tema1.grpc.MedicationPlan.Builder.class);
    }

    // Construct using com.example.tema1.grpc.MedicationPlan.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
        getMedicationPlanFieldBuilder();
      }
    }
    public Builder clear() {
      super.clear();
      if (medicationPlanBuilder_ == null) {
        medicationPlan_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
      } else {
        medicationPlanBuilder_.clear();
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationPlan_descriptor;
    }

    public com.example.tema1.grpc.MedicationPlan getDefaultInstanceForType() {
      return com.example.tema1.grpc.MedicationPlan.getDefaultInstance();
    }

    public com.example.tema1.grpc.MedicationPlan build() {
      com.example.tema1.grpc.MedicationPlan result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.example.tema1.grpc.MedicationPlan buildPartial() {
      com.example.tema1.grpc.MedicationPlan result = new com.example.tema1.grpc.MedicationPlan(this);
      int from_bitField0_ = bitField0_;
      if (medicationPlanBuilder_ == null) {
        if (((bitField0_ & 0x00000001) == 0x00000001)) {
          medicationPlan_ = java.util.Collections.unmodifiableList(medicationPlan_);
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.medicationPlan_ = medicationPlan_;
      } else {
        result.medicationPlan_ = medicationPlanBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.example.tema1.grpc.MedicationPlan) {
        return mergeFrom((com.example.tema1.grpc.MedicationPlan)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.example.tema1.grpc.MedicationPlan other) {
      if (other == com.example.tema1.grpc.MedicationPlan.getDefaultInstance()) return this;
      if (medicationPlanBuilder_ == null) {
        if (!other.medicationPlan_.isEmpty()) {
          if (medicationPlan_.isEmpty()) {
            medicationPlan_ = other.medicationPlan_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureMedicationPlanIsMutable();
            medicationPlan_.addAll(other.medicationPlan_);
          }
          onChanged();
        }
      } else {
        if (!other.medicationPlan_.isEmpty()) {
          if (medicationPlanBuilder_.isEmpty()) {
            medicationPlanBuilder_.dispose();
            medicationPlanBuilder_ = null;
            medicationPlan_ = other.medicationPlan_;
            bitField0_ = (bitField0_ & ~0x00000001);
            medicationPlanBuilder_ = 
              com.google.protobuf.GeneratedMessageV3.alwaysUseFieldBuilders ?
                 getMedicationPlanFieldBuilder() : null;
          } else {
            medicationPlanBuilder_.addAllMessages(other.medicationPlan_);
          }
        }
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.example.tema1.grpc.MedicationPlan parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.example.tema1.grpc.MedicationPlan) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private java.util.List<com.example.tema1.grpc.IntakeGrpc> medicationPlan_ =
      java.util.Collections.emptyList();
    private void ensureMedicationPlanIsMutable() {
      if (!((bitField0_ & 0x00000001) == 0x00000001)) {
        medicationPlan_ = new java.util.ArrayList<com.example.tema1.grpc.IntakeGrpc>(medicationPlan_);
        bitField0_ |= 0x00000001;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilderV3<
        com.example.tema1.grpc.IntakeGrpc, com.example.tema1.grpc.IntakeGrpc.Builder, com.example.tema1.grpc.IntakeGrpcOrBuilder> medicationPlanBuilder_;

    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public java.util.List<com.example.tema1.grpc.IntakeGrpc> getMedicationPlanList() {
      if (medicationPlanBuilder_ == null) {
        return java.util.Collections.unmodifiableList(medicationPlan_);
      } else {
        return medicationPlanBuilder_.getMessageList();
      }
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public int getMedicationPlanCount() {
      if (medicationPlanBuilder_ == null) {
        return medicationPlan_.size();
      } else {
        return medicationPlanBuilder_.getCount();
      }
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public com.example.tema1.grpc.IntakeGrpc getMedicationPlan(int index) {
      if (medicationPlanBuilder_ == null) {
        return medicationPlan_.get(index);
      } else {
        return medicationPlanBuilder_.getMessage(index);
      }
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder setMedicationPlan(
        int index, com.example.tema1.grpc.IntakeGrpc value) {
      if (medicationPlanBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureMedicationPlanIsMutable();
        medicationPlan_.set(index, value);
        onChanged();
      } else {
        medicationPlanBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder setMedicationPlan(
        int index, com.example.tema1.grpc.IntakeGrpc.Builder builderForValue) {
      if (medicationPlanBuilder_ == null) {
        ensureMedicationPlanIsMutable();
        medicationPlan_.set(index, builderForValue.build());
        onChanged();
      } else {
        medicationPlanBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder addMedicationPlan(com.example.tema1.grpc.IntakeGrpc value) {
      if (medicationPlanBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureMedicationPlanIsMutable();
        medicationPlan_.add(value);
        onChanged();
      } else {
        medicationPlanBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder addMedicationPlan(
        int index, com.example.tema1.grpc.IntakeGrpc value) {
      if (medicationPlanBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureMedicationPlanIsMutable();
        medicationPlan_.add(index, value);
        onChanged();
      } else {
        medicationPlanBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder addMedicationPlan(
        com.example.tema1.grpc.IntakeGrpc.Builder builderForValue) {
      if (medicationPlanBuilder_ == null) {
        ensureMedicationPlanIsMutable();
        medicationPlan_.add(builderForValue.build());
        onChanged();
      } else {
        medicationPlanBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder addMedicationPlan(
        int index, com.example.tema1.grpc.IntakeGrpc.Builder builderForValue) {
      if (medicationPlanBuilder_ == null) {
        ensureMedicationPlanIsMutable();
        medicationPlan_.add(index, builderForValue.build());
        onChanged();
      } else {
        medicationPlanBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder addAllMedicationPlan(
        java.lang.Iterable<? extends com.example.tema1.grpc.IntakeGrpc> values) {
      if (medicationPlanBuilder_ == null) {
        ensureMedicationPlanIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, medicationPlan_);
        onChanged();
      } else {
        medicationPlanBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder clearMedicationPlan() {
      if (medicationPlanBuilder_ == null) {
        medicationPlan_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
      } else {
        medicationPlanBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public Builder removeMedicationPlan(int index) {
      if (medicationPlanBuilder_ == null) {
        ensureMedicationPlanIsMutable();
        medicationPlan_.remove(index);
        onChanged();
      } else {
        medicationPlanBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public com.example.tema1.grpc.IntakeGrpc.Builder getMedicationPlanBuilder(
        int index) {
      return getMedicationPlanFieldBuilder().getBuilder(index);
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public com.example.tema1.grpc.IntakeGrpcOrBuilder getMedicationPlanOrBuilder(
        int index) {
      if (medicationPlanBuilder_ == null) {
        return medicationPlan_.get(index);  } else {
        return medicationPlanBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public java.util.List<? extends com.example.tema1.grpc.IntakeGrpcOrBuilder> 
         getMedicationPlanOrBuilderList() {
      if (medicationPlanBuilder_ != null) {
        return medicationPlanBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(medicationPlan_);
      }
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public com.example.tema1.grpc.IntakeGrpc.Builder addMedicationPlanBuilder() {
      return getMedicationPlanFieldBuilder().addBuilder(
          com.example.tema1.grpc.IntakeGrpc.getDefaultInstance());
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public com.example.tema1.grpc.IntakeGrpc.Builder addMedicationPlanBuilder(
        int index) {
      return getMedicationPlanFieldBuilder().addBuilder(
          index, com.example.tema1.grpc.IntakeGrpc.getDefaultInstance());
    }
    /**
     * <code>repeated .com.example.tema1.grpc.IntakeGrpc medicationPlan = 1;</code>
     */
    public java.util.List<com.example.tema1.grpc.IntakeGrpc.Builder> 
         getMedicationPlanBuilderList() {
      return getMedicationPlanFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilderV3<
        com.example.tema1.grpc.IntakeGrpc, com.example.tema1.grpc.IntakeGrpc.Builder, com.example.tema1.grpc.IntakeGrpcOrBuilder> 
        getMedicationPlanFieldBuilder() {
      if (medicationPlanBuilder_ == null) {
        medicationPlanBuilder_ = new com.google.protobuf.RepeatedFieldBuilderV3<
            com.example.tema1.grpc.IntakeGrpc, com.example.tema1.grpc.IntakeGrpc.Builder, com.example.tema1.grpc.IntakeGrpcOrBuilder>(
                medicationPlan_,
                ((bitField0_ & 0x00000001) == 0x00000001),
                getParentForChildren(),
                isClean());
        medicationPlan_ = null;
      }
      return medicationPlanBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:com.example.tema1.grpc.MedicationPlan)
  }

  // @@protoc_insertion_point(class_scope:com.example.tema1.grpc.MedicationPlan)
  private static final com.example.tema1.grpc.MedicationPlan DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.example.tema1.grpc.MedicationPlan();
  }

  public static com.example.tema1.grpc.MedicationPlan getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<MedicationPlan>
      PARSER = new com.google.protobuf.AbstractParser<MedicationPlan>() {
    public MedicationPlan parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new MedicationPlan(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<MedicationPlan> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<MedicationPlan> getParserForType() {
    return PARSER;
  }

  public com.example.tema1.grpc.MedicationPlan getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

