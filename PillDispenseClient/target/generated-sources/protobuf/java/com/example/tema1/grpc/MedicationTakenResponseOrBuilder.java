// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: HelloService.proto

package com.example.tema1.grpc;

public interface MedicationTakenResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.example.tema1.grpc.MedicationTakenResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string taken = 1;</code>
   */
  java.lang.String getTaken();
  /**
   * <code>string taken = 1;</code>
   */
  com.google.protobuf.ByteString
      getTakenBytes();
}
