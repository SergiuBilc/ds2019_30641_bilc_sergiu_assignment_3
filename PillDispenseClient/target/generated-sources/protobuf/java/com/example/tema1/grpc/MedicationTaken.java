// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: HelloService.proto

package com.example.tema1.grpc;

/**
 * Protobuf type {@code com.example.tema1.grpc.MedicationTaken}
 */
public  final class MedicationTaken extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:com.example.tema1.grpc.MedicationTaken)
    MedicationTakenOrBuilder {
  // Use MedicationTaken.newBuilder() to construct.
  private MedicationTaken(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private MedicationTaken() {
    intakeId_ = 0;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private MedicationTaken(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 8: {

            intakeId_ = input.readInt32();
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationTaken_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationTaken_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.example.tema1.grpc.MedicationTaken.class, com.example.tema1.grpc.MedicationTaken.Builder.class);
  }

  public static final int INTAKEID_FIELD_NUMBER = 1;
  private int intakeId_;
  /**
   * <code>int32 intakeId = 1;</code>
   */
  public int getIntakeId() {
    return intakeId_;
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (intakeId_ != 0) {
      output.writeInt32(1, intakeId_);
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (intakeId_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(1, intakeId_);
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.example.tema1.grpc.MedicationTaken)) {
      return super.equals(obj);
    }
    com.example.tema1.grpc.MedicationTaken other = (com.example.tema1.grpc.MedicationTaken) obj;

    boolean result = true;
    result = result && (getIntakeId()
        == other.getIntakeId());
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + INTAKEID_FIELD_NUMBER;
    hash = (53 * hash) + getIntakeId();
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.example.tema1.grpc.MedicationTaken parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationTaken parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.example.tema1.grpc.MedicationTaken parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.example.tema1.grpc.MedicationTaken parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.example.tema1.grpc.MedicationTaken prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.example.tema1.grpc.MedicationTaken}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.example.tema1.grpc.MedicationTaken)
      com.example.tema1.grpc.MedicationTakenOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationTaken_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationTaken_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.example.tema1.grpc.MedicationTaken.class, com.example.tema1.grpc.MedicationTaken.Builder.class);
    }

    // Construct using com.example.tema1.grpc.MedicationTaken.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      intakeId_ = 0;

      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.example.tema1.grpc.HelloService.internal_static_com_example_tema1_grpc_MedicationTaken_descriptor;
    }

    public com.example.tema1.grpc.MedicationTaken getDefaultInstanceForType() {
      return com.example.tema1.grpc.MedicationTaken.getDefaultInstance();
    }

    public com.example.tema1.grpc.MedicationTaken build() {
      com.example.tema1.grpc.MedicationTaken result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.example.tema1.grpc.MedicationTaken buildPartial() {
      com.example.tema1.grpc.MedicationTaken result = new com.example.tema1.grpc.MedicationTaken(this);
      result.intakeId_ = intakeId_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.example.tema1.grpc.MedicationTaken) {
        return mergeFrom((com.example.tema1.grpc.MedicationTaken)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.example.tema1.grpc.MedicationTaken other) {
      if (other == com.example.tema1.grpc.MedicationTaken.getDefaultInstance()) return this;
      if (other.getIntakeId() != 0) {
        setIntakeId(other.getIntakeId());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.example.tema1.grpc.MedicationTaken parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.example.tema1.grpc.MedicationTaken) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private int intakeId_ ;
    /**
     * <code>int32 intakeId = 1;</code>
     */
    public int getIntakeId() {
      return intakeId_;
    }
    /**
     * <code>int32 intakeId = 1;</code>
     */
    public Builder setIntakeId(int value) {
      
      intakeId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>int32 intakeId = 1;</code>
     */
    public Builder clearIntakeId() {
      
      intakeId_ = 0;
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:com.example.tema1.grpc.MedicationTaken)
  }

  // @@protoc_insertion_point(class_scope:com.example.tema1.grpc.MedicationTaken)
  private static final com.example.tema1.grpc.MedicationTaken DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.example.tema1.grpc.MedicationTaken();
  }

  public static com.example.tema1.grpc.MedicationTaken getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<MedicationTaken>
      PARSER = new com.google.protobuf.AbstractParser<MedicationTaken>() {
    public MedicationTaken parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new MedicationTaken(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<MedicationTaken> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<MedicationTaken> getParserForType() {
    return PARSER;
  }

  public com.example.tema1.grpc.MedicationTaken getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

