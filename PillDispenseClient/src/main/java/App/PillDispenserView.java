package App;


import com.example.tema1.grpc.IntakeGrpc;
import com.example.tema1.grpc.MedicationPlan;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.awt.Component.CENTER_ALIGNMENT;


public class PillDispenserView {
    private final JLabel timeLabel;
    private final JLabel dateLabel;
    private final JPanel buttonsPanel;
    private MedicationPlan medicationPlan;
    Calendar now;
    Date simulatedTime;

    HashMap<String, SomeAction> buttons = new HashMap<String, SomeAction>();
    Controller controller;
    protected SimpleDateFormat dateFormat =
            new SimpleDateFormat("EEE, d MMM yyyy");
    protected SimpleDateFormat timeFormat =
            new SimpleDateFormat("HH:mm:ss");
    private Calendar midnight;

    public PillDispenserView() {

        midnight = Calendar.getInstance();

        JFrame frame = new JFrame();
        frame.setBounds(100, 200, 400, 200);
        frame.setTitle("Pill Dispenser");

        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new BorderLayout());

        JStatusBar statusBar = new JStatusBar();
        JLabel leftLabel = new JLabel("The dispenser is online.");
        statusBar.setLeftComponent(leftLabel);

        dateLabel = new JLabel();
        dateLabel.setHorizontalAlignment(JLabel.CENTER);
        statusBar.addRightComponent(dateLabel);

        timeLabel = new JLabel();
        timeLabel.setHorizontalAlignment(JLabel.CENTER);
        statusBar.addRightComponent(timeLabel);

        contentPane.add(statusBar, BorderLayout.SOUTH);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
        buttonsPanel.setAlignmentX(CENTER_ALIGNMENT);
        buttonsPanel.setMaximumSize(new Dimension(200, 200));

        controller = new Controller(this);


        String limit = "00:00:00";
        String[] parts = limit.split(":");
        midnight.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
        midnight.add(Calendar.DATE, 1);
        midnight.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
        midnight.set(Calendar.SECOND, Integer.parseInt(parts[2]));


        simulatedTime = new Date();

        new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Instant aux = simulatedTime.toInstant();

                simulatedTime = new Date(aux.plus(300, ChronoUnit.SECONDS).toEpochMilli());
                dateLabel.setText(dateFormat.format(simulatedTime));
                timeLabel.setText(timeFormat.format(simulatedTime));

                now = Calendar.getInstance();


                String actual = timeFormat.format(simulatedTime);
                Calendar currentTime = Calendar.getInstance();
                currentTime = computeCalendar(currentTime, actual, Integer.parseInt((dateFormat.format(simulatedTime)).split(" ")[1]));



                if (currentTime.after(midnight)) {

                    medicationPlan = controller.getMedicationPlan(Calendar.getInstance().getTime());
                    buttons = new  HashMap<String, SomeAction>();
                    System.out.println(medicationPlan.toString());
                    midnight.add(Calendar.DATE, 1);
                }
                if (medicationPlan != null && medicationPlan.isInitialized()) {

                    for (int i = 0; i < medicationPlan.getMedicationPlanList().size(); i++) {
                        IntakeGrpc intake = medicationPlan.getMedicationPlanList().get(i);

                        actual = timeFormat.format(intake.getStartTime());
                        Calendar intakeStartTime = Calendar.getInstance();
                        intakeStartTime = computeCalendar(intakeStartTime, actual, Integer.parseInt((dateFormat.format(simulatedTime)).split(" ")[1]));


                        //System.out.println( dateFormat.format(currentTime.getTime()) + " " +timeFormat.format(currentTime.getTime()) + " ||||||  "+ dateFormat.format(intakeStartTime.getTime()) + " " +timeFormat.format(intakeStartTime.getTime()));

                        if (currentTime.after(intakeStartTime) && buttons.get(intake.getName()) == null) {
                            JButton button = new JButton();
                            SomeAction action = new SomeAction(intake.getName(), intake.getIntakeId(), button);
                            buttons.put(intake.getName(), action);
                            buttonsPanel.add(button);
                        }

                        actual = timeFormat.format(intake.getEndTime());
                        Calendar intakeEndTime = Calendar.getInstance();
                        intakeEndTime = computeCalendar(intakeStartTime, actual, Integer.parseInt((dateFormat.format(simulatedTime)).split(" ")[1]));

                        if (currentTime.after(intakeEndTime) && buttons.get(intake.getName()).buttonPressed == false ) {
                            controller.setMedicationAsNotTaken(intake.getIntakeId());
                            buttonsPanel.remove(buttons.get(intake.getName()).button);
                            buttons.remove(e.getActionCommand());
                            buttons.get(intake.getName()).buttonPressed=true;
                            buttonsPanel.updateUI();
                        }
                    }
                }


            }
        }).start();


        // contentPane.add(lablel);
        contentPane.add(buttonsPanel, BorderLayout.NORTH);
        frame.repaint();
        frame.setVisible(true);

    }

    private Calendar computeCalendar(Calendar initialCalendar, String time, Integer date) {
        String[] parts = time.split(":");
        initialCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
        initialCalendar.set(Calendar.DATE, date);
        initialCalendar.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
        initialCalendar.set(Calendar.SECOND, Integer.parseInt(parts[2]));
        return initialCalendar;
    }

    private class SomeAction extends AbstractAction {
        private int intakeId;
        private JButton button;
        private String name;
        private boolean buttonPressed = false;

        public SomeAction(String name, int intakeId, JButton button) {
            super("Take " + name);
            this.intakeId = intakeId;
            this.button = button;
            this.name = name;
            this.button.setText("Take " + name);
            this.button.addActionListener(this);
        }


        @Override
        public void actionPerformed(ActionEvent e) {
            controller.takeMedication(intakeId);
            buttonsPanel.remove(buttons.get(name).button);
            buttons.remove(e.getActionCommand());
            buttonPressed = true;
            buttonsPanel.updateUI();


        }
    }

}

