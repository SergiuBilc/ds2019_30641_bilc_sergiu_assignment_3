package App;

import com.example.tema1.grpc.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class Controller {
    private PillDispenserView view;
    private MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stub;
    public Controller(PillDispenserView v){
        this.view = v;
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8081)
                .usePlaintext()
                .build();

        stub = MedicationPlanServiceGrpc.newBlockingStub(channel);
    }

    public MedicationTakenResponse takeMedication(Integer intakeId) {
        System.out.println(intakeId);
        MedicationTakenResponse medicationTakenResponse = stub.medicationTaken(MedicationTaken.newBuilder().setIntakeId(intakeId).build());
        System.out.println(medicationTakenResponse.getTaken());
        return medicationTakenResponse;
    }

    public MedicationTakenResponse setMedicationAsNotTaken(Integer intakeId) {
        System.out.println(intakeId);
        MedicationTakenResponse medicationTakenResponse = stub.medicationNotTaken(MedicationTaken.newBuilder().setIntakeId(intakeId).build());
        System.out.println(medicationTakenResponse.getTaken());
        return medicationTakenResponse;
    }

    public MedicationPlan getMedicationPlan(Date date) {
        MedicationPlan medicationPlanResponse = stub.getMedicationPlan(MedicationPlanRequest.newBuilder()
                .setPatientId(1)
                .setDate(date.getTime())
                .build());
        return medicationPlanResponse;
    }

}
