package ds.firstassignment.controller;

import ds.firstassignment.dto.UserDTO;
import ds.firstassignment.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("http://localhost:4200")
@Controller
@RequestMapping(value="/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping()
    public ResponseEntity<?> login(@RequestBody UserDTO userDTO){
        try {
            UserDTO userDto = loginService.login(userDTO.getUsername() ,  userDTO.getPassword());
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }

    }

}
