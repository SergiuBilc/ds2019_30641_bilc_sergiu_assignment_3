package ds.firstassignment.controller;


import ds.firstassignment.dto.IntakeDTO;
import ds.firstassignment.dto.PatientDTO;
import ds.firstassignment.dto.UserDTO;
import ds.firstassignment.entities.Intake;
import ds.firstassignment.services.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@Controller
@RequestMapping(value = "/intake")
public class IntakeController {

    @Autowired
    private IntakeService intakeService;

    @PostMapping(value = "/add")
    public ResponseEntity<?> addIntake(@RequestBody IntakeDTO intakeDTO) {
        try {
            intakeService.addIntake(intakeDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteIntake(@RequestBody IntakeDTO intakeDTO) {
        try {
            intakeService.deleteIntake(intakeDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value="/update")
    public ResponseEntity<?> updateIntake(@RequestBody IntakeDTO intakeDTO) {
        try {
            IntakeDTO intake = intakeService.updateIntake(intakeDTO);
            return new ResponseEntity<>(intake, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/byPatient")
    public ResponseEntity<?> getIntakesByPatient(@RequestBody PatientDTO patientDTO) {
        try {
            List<IntakeDTO> intakes = intakeService.findIntakesByPatient(patientDTO);
            return new ResponseEntity<>(intakes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

}
