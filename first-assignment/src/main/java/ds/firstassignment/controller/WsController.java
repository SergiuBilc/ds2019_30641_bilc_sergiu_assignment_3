package ds.firstassignment.controller;


import doctor.wsdl.*;

import ds.firstassignment.dto.CheckIfTakenDto;
import ds.firstassignment.dto.RecommendationDto;
import ds.firstassignment.ws.Client;
import ds.firstassignment.ws.WsConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBElement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin("http://localhost:4200")
@Controller
@RequestMapping(value = "/wsDoctor")
public class WsController {

    @PostMapping(value="/getActivities")
    public ResponseEntity<?> getActivities(@RequestParam Integer idPatient, @RequestBody CheckIfTakenDto date) {
        try {
            Date dateFromString = new SimpleDateFormat("yyyy-MM-dd").parse(date.getDate());
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(WsConfig.class);
            Client client = context.getBean(Client.class);
            JAXBElement<GetActivitiesForPatientResponse> response = client.getActivities(idPatient, dateFromString);
//            System.out.println(response.getValue().getReturn());
            return new ResponseEntity<>(response.getValue().getReturn(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/checkIfTaken")
    public ResponseEntity<?> checkIfTaken(@RequestParam Integer idPatient, @RequestBody CheckIfTakenDto date) {
        try {
            Date dateFromString = new SimpleDateFormat("yyyy-MM-dd").parse(date.getDate());
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(WsConfig.class);
            Client client = context.getBean(Client.class);
            JAXBElement<CheckIfMedicationTakenResponse> response = client.checkIfMedicationTaken(idPatient, dateFromString);
            return new ResponseEntity<>(response.getValue(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/getNonNormalActivities")
    public ResponseEntity<?> getNonNormalActivities(@RequestParam Integer idPatient) {
        try {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(WsConfig.class);
            Client client = context.getBean(Client.class);
            JAXBElement<GetNonNormalActivitiesResponse> response = client.getNonNormalActivities(idPatient);
            return new ResponseEntity<>(response.getValue(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value="/annotateActivity")
    public ResponseEntity<?> annotateActivity(@RequestParam Integer activityId, @RequestParam boolean normal) {
        try {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(WsConfig.class);
            Client client = context.getBean(Client.class);
            JAXBElement<AnnotateActivityResponse> response = client.annotateActivity(activityId, normal);
            return new ResponseEntity<>(response.getValue(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value="/addRecommendation")
    public ResponseEntity<?> annotateActivity(@RequestParam Integer patientId, @RequestBody RecommendationDto recommendation) {
        try {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(WsConfig.class);
            Client client = context.getBean(Client.class);
            JAXBElement<AddRecommendationResponse> response = client.addRecommendation(patientId, recommendation.getRecommendation());
            return new ResponseEntity<>(response.getValue(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }
}
