package ds.firstassignment;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateXmlAdapter extends XmlAdapter<String, Date> {

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    //public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);


    @Override
    public Date unmarshal(String v) throws Exception {
        Date date = new Date();
        date = new SimpleDateFormat(DATE_FORMAT).parse(v);
        return date;
    }

    public String marshal(Date v) throws Exception {

        String result = new SimpleDateFormat(DATE_FORMAT).format(v);
        return result ;
    }
}