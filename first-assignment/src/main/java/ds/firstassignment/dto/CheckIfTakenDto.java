package ds.firstassignment.dto;

public class CheckIfTakenDto {

    String date;

    public CheckIfTakenDto() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
