package ds.firstassignment.dto;

import java.sql.Date;
import java.time.Instant;

public class IntakeDTO {
    private Integer id;
    private Date startDate;
    private Date endDate;
    private String details;
    private MedicationDTO medication;
    private Instant startTime;
    private Instant endTime;

    public IntakeDTO() {
    }

    public IntakeDTO(Integer id, Date startDate, Date endDate, String details) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.details = details;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public MedicationDTO getMedication() {
        return medication;
    }

    public void setMedication(MedicationDTO medication) {
        this.medication = medication;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }
}
