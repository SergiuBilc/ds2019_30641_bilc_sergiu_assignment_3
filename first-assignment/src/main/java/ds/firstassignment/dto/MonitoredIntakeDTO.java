package ds.firstassignment.dto;

import java.sql.Date;

public class MonitoredIntakeDTO {

    private Integer id;
    private Date date;
    private Boolean taken;
    private IntakeDTO intake;

    public MonitoredIntakeDTO() {
    }

    public MonitoredIntakeDTO(Integer id, Date date, Boolean taken) {
        this.id = id;
        this.date = date;
        this.taken = taken;
    }

    public MonitoredIntakeDTO(Integer id, Date date, Boolean taken, IntakeDTO intake) {
        this.id = id;
        this.date = date;
        this.taken = taken;
        this.intake = intake;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }

    public IntakeDTO getIntake() {
        return intake;
    }

    public void setIntake(IntakeDTO intake) {
        this.intake = intake;
    }
}
