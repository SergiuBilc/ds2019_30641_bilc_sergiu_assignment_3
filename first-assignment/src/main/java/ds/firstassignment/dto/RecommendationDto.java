package ds.firstassignment.dto;

public class RecommendationDto {

    String recommendation;

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }
}
