package ds.firstassignment.dto;

import ds.firstassignment.entities.Medication;

import java.sql.Date;

public class IntakeWithMedicationDTO {
    private Integer id;
    private Date startDate;
    private Date endDate;
    private String details;
    private MedicationDTO medication;

    public IntakeWithMedicationDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public MedicationDTO getMedication() {
        return medication;
    }

    public void setMedication(MedicationDTO medication) {
        this.medication = medication;
    }
}
