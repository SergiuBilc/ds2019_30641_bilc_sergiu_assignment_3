package ds.firstassignment.dto;

import java.sql.Date;
import java.util.List;

public class PatientWithMedicationDTO {


    private Integer id;
    private String name;
    private Date birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private List<IntakeWithMedicationDTO> medicationPlans;

    public PatientWithMedicationDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<IntakeWithMedicationDTO> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<IntakeWithMedicationDTO> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
