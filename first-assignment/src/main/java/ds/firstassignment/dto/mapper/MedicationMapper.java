package ds.firstassignment.dto.mapper;

import ds.firstassignment.dto.MedicationDTO;
import ds.firstassignment.dto.MedicationViewDTO;
import ds.firstassignment.entities.Medication;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MedicationMapper {
    Medication medicationDTOtoMedication(MedicationDTO medicationDTO);
    MedicationDTO medicationToMedicationDTO(Medication medication);
    List<MedicationDTO> medicationListtoMedicationDTOList (List<Medication> medications);
    List<Medication> medicationDTOListtoMedicationList (List<MedicationDTO> medicationDTOS);
    Medication medicationViewDTOtoMedication(MedicationViewDTO medicationViewDTO);
    MedicationViewDTO medicationToMedicationViewDTO(Medication medication);
}