package ds.firstassignment.dto.mapper;

import ds.firstassignment.dto.MonitoredIntakeDTO;
import ds.firstassignment.entities.MonitoredIntake;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MonitoredIntakeMapper {


    MonitoredIntake dtoToEntity(MonitoredIntakeDTO intakeDTO);
    MonitoredIntakeDTO entityToDTO(MonitoredIntake intake);
    List<MonitoredIntakeDTO> entityListToDtoList(List<MonitoredIntake> monitoredIntakes);

}
