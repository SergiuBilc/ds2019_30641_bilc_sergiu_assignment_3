package ds.firstassignment.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import ds.firstassignment.TimestampXmlAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Timestamp;

import static ds.firstassignment.TimestampXmlAdapter.DATE_FORMAT;
import static javax.persistence.GenerationType.IDENTITY;



@Entity
@Table(name="Activity")
@XmlAccessorType(XmlAccessType.FIELD)
public class Activity {


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String activity;

    @Column(name = "start", length = 100)
    @XmlJavaTypeAdapter(TimestampXmlAdapter.class)
    @JsonFormat(pattern = DATE_FORMAT)
    private Timestamp start;

    @Column(name = "end", length = 100)
    @XmlJavaTypeAdapter(TimestampXmlAdapter.class)
    @JsonFormat(pattern = DATE_FORMAT)
    private Timestamp end;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    @XmlTransient
    private Patient patient;

    @Column(name = "verified")
    private Boolean verified;

    @Column(name="status")
    private Boolean activityStatus;

    public Activity() {
    }

    public Activity(String activity, Timestamp start, Timestamp end, Patient patient) {
        this.activity = activity;
        this.start = start;
        this.end = end;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Boolean getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Boolean activityStatus) {
        this.activityStatus = activityStatus;
    }


    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", activity='" + activity + '\'' +
                ", startTime=" + start +
                ", endTime=" + end +
                ", patient=" + patient +
                '}';
    }
}
