package ds.firstassignment.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import ds.firstassignment.DateXmlAdapter;
import ds.firstassignment.TimestampXmlAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;

import static ds.firstassignment.TimestampXmlAdapter.DATE_FORMAT;
import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "monitored_intake")
@XmlAccessorType(XmlAccessType.FIELD)
public class MonitoredIntake {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "date")
    @XmlJavaTypeAdapter(DateXmlAdapter.class)
    @JsonFormat(pattern = DATE_FORMAT)
    private Date date;

    @Column(name = "taken")
    private Boolean taken;

    @ManyToOne
    @JoinColumn( name = "monitored_intake_id")
    private Intake intake;

    public MonitoredIntake() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Intake getIntake() {
        return intake;
    }

    public void setIntake(Intake intake) {
        this.intake = intake;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }
}
