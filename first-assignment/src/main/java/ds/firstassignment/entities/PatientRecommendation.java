package ds.firstassignment.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient_recommendation")
public class PatientRecommendation {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    @XmlTransient
    private Patient patient;

    @Column(name = "recommendation")
    private String recommendation;

    public PatientRecommendation() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }
}
