package ds.firstassignment.grpc;

import com.example.tema1.grpc.*;
import ds.firstassignment.dto.IntakeDTO;
import ds.firstassignment.entities.Intake;
import ds.firstassignment.entities.MonitoredIntake;
import ds.firstassignment.services.IntakeService;
import ds.firstassignment.services.MonitoredIntakeService;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.time.Instant;
import java.util.List;

public class MedicationPlanServiceImpl extends MedicationPlanServiceGrpc.MedicationPlanServiceImplBase {

    @Autowired
    IntakeService intakeService;

    @Autowired
    MonitoredIntakeService monitoredIntakeService;


    @Override
    public void getMedicationPlan(MedicationPlanRequest request, StreamObserver<MedicationPlan> responseObserver) {
        List<IntakeDTO> medicationPlan = intakeService.findIntakesByPatientId(request.getPatientId());
        MedicationPlan medPlanResponse = MedicationPlan.newBuilder().build();

        for (int j = 0; j < medicationPlan.size(); j++) {
            IntakeDTO intakeDTO = medicationPlan.get(j);

            if (intakeDTO.getStartDate().getTime() < request.getDate() && request.getDate() < intakeDTO.getEndDate().getTime()) {
                IntakeGrpc intakeGrpc = IntakeGrpc.newBuilder()
                        .setIntakeId(intakeDTO.getId())
                        .setStartTime(intakeDTO.getStartTime().toEpochMilli())
                        .setEndTime(intakeDTO.getEndTime().toEpochMilli())
                        .setName(intakeDTO.getMedication().getName())
                        .build();
                medPlanResponse = MedicationPlan.newBuilder(medPlanResponse).addMedicationPlan(intakeGrpc).build();
            }
        }

        responseObserver.onNext(medPlanResponse);
        responseObserver.onCompleted();
    }


    @Override
    public void medicationTaken(MedicationTaken request, StreamObserver<MedicationTakenResponse> responseObserver) {
        MonitoredIntake monitoredIntake = getMonitoredIntake(request);
        monitoredIntake.setTaken(true);

        monitoredIntakeService.addMonitoredIntake(monitoredIntake);

        responseObserver.onNext(MedicationTakenResponse.newBuilder().setTaken("Medication taken!").build());
        responseObserver.onCompleted();
    }

    @Override
    public void medicationNotTaken(MedicationTaken request, StreamObserver<MedicationTakenResponse> responseObserver) {
        MonitoredIntake monitoredIntake = getMonitoredIntake(request);
        monitoredIntake.setTaken(false);

        monitoredIntakeService.addMonitoredIntake(monitoredIntake);

        responseObserver.onNext(MedicationTakenResponse.newBuilder().setTaken("Medication not taken!").build());
        responseObserver.onCompleted();
    }

    private MonitoredIntake getMonitoredIntake(MedicationTaken request) {
        MonitoredIntake monitoredIntake = new MonitoredIntake();
        Intake intake = intakeService.findEntityById(request.getIntakeId());
        monitoredIntake.setIntake(intake);
        monitoredIntake.setDate(new Date(Instant.now().toEpochMilli()));
        return monitoredIntake;
    }
}
