package ds.firstassignment;

import ds.firstassignment.grpc.MedicationPlanServiceImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.net.URL;

@SpringBootApplication
public class FirstAssignmentApplication implements CommandLineRunner {


    @Autowired
    MedicationPlanServiceImpl medicationPlanService;


    public static void main(String[] args) throws IOException, InterruptedException {
        SpringApplication.run(FirstAssignmentApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {


        Server server = ServerBuilder.forPort(8082)
                .addService(this.medicationPlanService).build();

        System.out.println("Starting server...");
        server.start();
        System.out.println("Server started!");
        server.awaitTermination();
    }


    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public MedicationPlanServiceImpl medicationPlanServiceImpl() {
        return new MedicationPlanServiceImpl();
    }

}
