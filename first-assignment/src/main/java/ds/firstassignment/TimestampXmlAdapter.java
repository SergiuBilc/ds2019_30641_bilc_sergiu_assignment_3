package ds.firstassignment;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimestampXmlAdapter extends XmlAdapter<String, Timestamp> {

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    //public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);


    @Override
    public Timestamp unmarshal(String v) throws Exception {
        Timestamp ts = Timestamp.valueOf(v);
        return ts;
    }

    public String marshal(Timestamp v) throws Exception {
        Date date = new Date();
        date.setTime(v.getTime());
        String result = new SimpleDateFormat(DATE_FORMAT).format(date);
        return result ;
    }
}