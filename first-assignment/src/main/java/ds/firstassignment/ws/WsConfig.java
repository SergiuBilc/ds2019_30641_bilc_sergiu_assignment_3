package ds.firstassignment.ws;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class WsConfig {

    //@Qualifier("DoctorMarshaller") Jaxb2Marshaller doctorMarshaller;
   // @Qualifier("CaregiverMarshaller") Jaxb2Marshaller caregiverMarshaller;

    @Bean(name = "DoctorMarshaller")
    public Jaxb2Marshaller doctorMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("doctor.wsdl");
        return marshaller;
    }

    @Bean
    public Client client(@Qualifier("DoctorMarshaller") Jaxb2Marshaller doctorMarshaller) {
        Client client = new Client();
        client.setDefaultUri("http://localhost:8081/doctorService");
        client.setMarshaller(doctorMarshaller);
        client.setUnmarshaller(doctorMarshaller);
        return client;
    }
//
//    @Bean(name = "CaregiverMarshaller")
//    public Jaxb2Marshaller caregiverMarshaller() {
//        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//        marshaller.setContextPath("caregiver.wsdl");
//        return marshaller;
//    }
//
//    @Bean
//    public ClientCaregiver clientCaregiver(@Qualifier("CaregiverMarshaller") Jaxb2Marshaller caregiverMarshaller) {
//        ClientCaregiver client = new ClientCaregiver();
//        client.setDefaultUri("http://localhost:50391/WebService1.asmx");
//        client.setMarshaller(caregiverMarshaller);
//        client.setUnmarshaller(caregiverMarshaller);
//        return client;
//    }
}
