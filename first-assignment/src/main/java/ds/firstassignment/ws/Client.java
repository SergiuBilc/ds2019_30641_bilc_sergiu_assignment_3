package ds.firstassignment.ws;

//import caregiver.wsdl.HelloWorldResponse;
import doctor.wsdl.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Client extends WebServiceGatewaySupport {


    public JAXBElement<GetActivitiesForPatientResponse> getActivities(int idPatient, Date date) throws DatatypeConfigurationException {
        ObjectFactory objectFactory = new ObjectFactory();
        GetActivitiesForPatient request = objectFactory.createGetActivitiesForPatient();
        request.setArg0(idPatient);
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        request.setArg1(xmlDate);

        JAXBElement<GetActivitiesForPatient> jaxbGetSubscriptionInfo = objectFactory.createGetActivitiesForPatient(request);
        JAXBElement<GetActivitiesForPatientResponse> response
                = (JAXBElement<GetActivitiesForPatientResponse>) getWebServiceTemplate()
                .marshalSendAndReceive(
                        jaxbGetSubscriptionInfo
                );

        return response;
    }

    public JAXBElement<CheckIfMedicationTakenResponse> checkIfMedicationTaken(int idPatient, Date date) throws DatatypeConfigurationException {

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        ObjectFactory objectFactory = new ObjectFactory();
        CheckIfMedicationTaken request = objectFactory.createCheckIfMedicationTaken();
        request.setArg0(idPatient);
        request.setArg1(xmlDate);

        JAXBElement<CheckIfMedicationTaken> jaxbGetSubscriptionInfo = objectFactory.createCheckIfMedicationTaken(request);
        JAXBElement<CheckIfMedicationTakenResponse> response
                = (JAXBElement<CheckIfMedicationTakenResponse>) getWebServiceTemplate()
                .marshalSendAndReceive(
                        jaxbGetSubscriptionInfo
                );
        return response;
    }

    public JAXBElement<GetNonNormalActivitiesResponse> getNonNormalActivities(int idPatient) {
        ObjectFactory objectFactory = new ObjectFactory();
        GetNonNormalActivities request = objectFactory.createGetNonNormalActivities();
        request.setArg0(idPatient);

        JAXBElement<GetNonNormalActivities> jaxbGetSubscriptionInfo = objectFactory.createGetNonNormalActivities(request);
        JAXBElement<GetNonNormalActivitiesResponse> response
                = (JAXBElement<GetNonNormalActivitiesResponse>) getWebServiceTemplate()
                .marshalSendAndReceive(
                        jaxbGetSubscriptionInfo
                );

        return response;
    }

    public JAXBElement<AnnotateActivityResponse> annotateActivity(int activityId, boolean normal) {
        ObjectFactory objectFactory = new ObjectFactory();
        AnnotateActivity request = objectFactory.createAnnotateActivity();
        request.setArg0(activityId);
        request.setArg1(normal);

        JAXBElement<AnnotateActivity> jaxbGetSubscriptionInfo = objectFactory.createAnnotateActivity(request);
        JAXBElement<AnnotateActivityResponse> response
                = (JAXBElement<AnnotateActivityResponse>) getWebServiceTemplate()
                .marshalSendAndReceive(
                        jaxbGetSubscriptionInfo
                );

        return response;
    }

    public JAXBElement<AddRecommendationResponse> addRecommendation(int patientId, String recommendation) {
        ObjectFactory objectFactory = new ObjectFactory();
        AddRecommendation request = objectFactory.createAddRecommendation();
        request.setArg0(patientId);
        request.setArg1(recommendation);

        JAXBElement<AddRecommendation> jaxbGetSubscriptionInfo = objectFactory.createAddRecommendation(request);
        JAXBElement<AddRecommendationResponse> response
                = (JAXBElement<AddRecommendationResponse>) getWebServiceTemplate()
                .marshalSendAndReceive(
                        jaxbGetSubscriptionInfo
                );

        return response;
    }

}
