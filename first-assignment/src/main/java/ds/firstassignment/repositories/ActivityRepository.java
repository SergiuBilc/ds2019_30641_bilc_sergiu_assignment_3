package ds.firstassignment.repositories;

import java.util.List;
import ds.firstassignment.entities.Activity;
import ds.firstassignment.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {

    List<Activity> getActivitiesByPatient(Patient patient);
}
