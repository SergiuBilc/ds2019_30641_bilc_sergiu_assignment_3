package ds.firstassignment.repositories;

import ds.firstassignment.entities.MonitoredIntake;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitoredIntakeRepository extends JpaRepository<MonitoredIntake, Integer> {
}
