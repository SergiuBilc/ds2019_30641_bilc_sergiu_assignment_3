package ds.firstassignment.repositories;

import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.Patient;
import ds.firstassignment.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

    public List<Patient> findByCaregiver(Caregiver caregiver);

    Patient findByUser(User user);
}
