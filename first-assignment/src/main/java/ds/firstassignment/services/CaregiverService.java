package ds.firstassignment.services;

import ds.firstassignment.dto.*;
import ds.firstassignment.dto.mapper.CaregiverMapper;
import ds.firstassignment.dto.mapper.DoctorMapper;
import ds.firstassignment.dto.mapper.PatientMapper;
import ds.firstassignment.dto.mapper.UserMapper;
import ds.firstassignment.entities.*;
import ds.firstassignment.repositories.CaregiverRepository;
import ds.firstassignment.repositories.DoctorRepository;
import ds.firstassignment.repositories.PatientRepository;
import ds.firstassignment.repositories.UserRepository;
import org.apache.catalina.users.MemoryUserDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaregiverService {

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private CaregiverMapper caregiverMapper;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;


    public void addCaregiver(CaregiverDTO caregiver) {
        this.caregiverRepository.save(this.caregiverMapper.fromCaregiverDTOtoCaregiverEntity(caregiver));
    }

    public CaregiverWithPatientsDTO updateCaregiver(CaregiverWithPatientsDTO caregiverDTO) {
        Caregiver caregiver = this.caregiverRepository.findById(caregiverDTO.getId()).get();

        if (caregiver != null) {
            caregiver.setName(caregiverDTO.getName());
            caregiver.setAddress(caregiverDTO.getAddress());
            caregiver.setBirthDate(caregiverDTO.getBirthDate());
            caregiver.setGender(caregiverDTO.getGender());

            if (caregiverDTO.getPatients() == null) {
                caregiver.setPatients(this.patientMapper.patientWithMedicationDTOtoPatientList(caregiverDTO.getPatients()));
            }
        }
        Caregiver careSaved = this.caregiverRepository.save(caregiver);
        return this.caregiverMapper.fromEntityToCaregiverPatientDTO(careSaved);
    }

    public void deleteCaregiver(CaregiverDTO caregiver) {
        Caregiver caregiver1 = this.caregiverRepository.findById(caregiver.getId()).get();
        this.userRepository.delete(caregiver1.getUser());
        this.caregiverRepository.delete(this.caregiverMapper.fromCaregiverDTOtoCaregiverEntity(caregiver));
    }

    public CaregiverDTO findById(Integer id) {
        return this.caregiverMapper.fromEntityToCaregiverDTO(this.caregiverRepository.findById(id).get());
    }

    public List<CaregiverDTO> findAll() {
        return this.caregiverMapper.fromEntityListToCaregiverDTOList(this.caregiverRepository.findAll());
    }

    public List<CaregiverDTO> findByDoctor(DoctorDTO doctorDTO) {
        Doctor doctor = this.doctorMapper.fromDoctorDTOtoDoctorEntity(doctorDTO);
        return this.caregiverMapper.fromEntityListToCaregiverDTOList(caregiverRepository.findByDoctor(doctor));
    }

    public CaregiverWithPatientsDTO addPatientToCaregiver(Integer idCaregiver, PatientWithMedicationDTO patientDTO) {
        Caregiver caregiver = this.caregiverRepository.findById(idCaregiver).get();
        Patient patient = this.patientRepository.findById(patientDTO.getId()).get();

        if (caregiver != null) {

            if (patient == null) {
                patient = this.patientMapper.patientWithMedicationDTOtoPatient(patientDTO);
                patient.setCaregiver(caregiver);
                patientRepository.save(patient);
            }
            caregiver.getPatients().add(patient);
            this.caregiverRepository.save(caregiver);
        }

        return this.caregiverMapper.fromEntityToCaregiverPatientDTO(caregiver);
    }

    public UserDTO findUserDTOByCaregiver(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = this.caregiverRepository.findById(caregiverDTO.getId()).get();

        return this.userMapper.fromEntityToUserDTO(caregiver.getUser());
    }

    public User findUserByCaregiver(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = this.caregiverRepository.findById(caregiverDTO.getId()).get();
        return caregiver.getUser();
    }


    public void addCaregiverAndUserWithDoctor(UserDTO userDTO, CaregiverDTO caregiverDTO, Integer doctorId) {
        Caregiver caregiver = getCaregiver(caregiverDTO);
        Doctor doctor = doctorRepository.findById(doctorId).get();
        User user = new User(userDTO.getUsername(), userDTO.getPassword(), userDTO.getRole());
        caregiver.setUser(user);
        caregiver.setDoctor(doctor);

        caregiverRepository.save(caregiver);
    }

    public void updateCaregiver(UserDTO userDTO, CaregiverDTO caregiverDTO, Integer idDoctor) {
        Caregiver caregiver = caregiverRepository.findById(caregiverDTO.getId()).get();
        User user = this.findUserByCaregiver(caregiverDTO);

        caregiver = updateFieldsCaregiver(caregiver, caregiverDTO);
        updateUserFields(userDTO, user);
        caregiver.setUser(user);

        Doctor doctor = this.doctorRepository.findById(idDoctor).get();
        if(doctor!=null){
            caregiver.setDoctor(doctor);
        }

        userRepository.save(user);
        caregiverRepository.save(caregiver);
    }


    private Caregiver getCaregiver(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = new Caregiver();
        updateFieldsCaregiver(caregiver, caregiverDTO);
        return caregiver;
    }

    private Caregiver updateFieldsCaregiver(Caregiver caregiver, CaregiverDTO caregiverDTO) {
        caregiver.setName(caregiverDTO.getName());
        caregiver.setAddress(caregiverDTO.getAddress());
        caregiver.setBirthDate(caregiverDTO.getBirthDate());
        caregiver.setGender(caregiverDTO.getGender());
        return caregiver;
    }


    private void updateUserFields(UserDTO userDTO, User user) {
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
    }

    public void deleteCaregiverById(Integer id) {
        Caregiver caregiver = caregiverRepository.findById(id).get();
        User user = userService.findEntityById(caregiver.getUser().getId());
        userService.deleteUser(user);
        caregiverRepository.delete(caregiver);
    }

    public CaregiverDTO findByUser(UserDTO user) {
            return caregiverMapper.fromEntityToCaregiverDTO(caregiverRepository.findByUser(userMapper.fromUserDTOtoUserEntity(user)));
        }

    public List<PatientDTO> findMyCaregivers(Integer id) {
        Caregiver caregiver = this.caregiverRepository.findById(id).get();
        List<Patient> caregivers = caregiver.getPatients();
        return this.patientMapper.patientListtoPatientDTOList(caregivers);

    }
}