package ds.firstassignment.services;

import ds.firstassignment.dto.*;
import ds.firstassignment.dto.mapper.CaregiverMapper;
import ds.firstassignment.dto.mapper.DoctorMapper;
import ds.firstassignment.dto.mapper.PatientMapper;
import ds.firstassignment.dto.mapper.UserMapper;
import ds.firstassignment.entities.*;
import ds.firstassignment.repositories.CaregiverRepository;
import ds.firstassignment.repositories.DoctorRepository;
import ds.firstassignment.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private CaregiverMapper caregiverMapper;

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private MedicationService medicationService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;


    public void addDoctor(DoctorDTO doctor) {
        this.doctorRepository.save(this.doctorMapper.fromDoctorDTOtoDoctorEntity(doctor));
    }

    public DoctorDTO updateDoctor(DoctorDTO doctorDTO) {
        Doctor doctor = this.doctorRepository.findById(doctorDTO.getId()).get();

        if (doctor != null) {
            doctor.setName(doctorDTO.getName());
            doctor.setAddress(doctorDTO.getAddress());

        }
        Doctor doctorSaved = this.doctorRepository.save(doctor);
        return this.doctorMapper.fromEntityToDoctorDTO(this.doctorRepository.save(doctorSaved));
    }

    public void deleteDoctor(DoctorDTO doctor) {
        this.doctorRepository.delete(this.doctorMapper.fromDoctorDTOtoDoctorEntity(doctor));
    }

    public DoctorDTO findById(Integer id) {
        return this.doctorMapper.fromEntityToDoctorDTO(this.doctorRepository.findById(id).get());
    }

    public List<DoctorDTO> findAll() {
        return this.doctorMapper.fromEntityToDoctorDTOList(this.doctorRepository.findAll());
    }

    public DoctorDTO addCaregiver(Integer doctorID, CaregiverWithPatientsDTO caregiverDTO) {
        Doctor doctor = this.doctorRepository.findById(doctorID).get();
        Caregiver caregiver = this.caregiverRepository.findById(caregiverDTO.getId()).get();
        if (doctor != null) {
            if (caregiver == null) {
                caregiver = this.caregiverMapper.fromCaregiverPatientDTOtoCaregiverEntity(caregiverDTO);
                caregiver.setDoctor(doctor);
                this.caregiverRepository.save(caregiver);
            }

            doctor.getCaregivers().add(caregiver);
        }

        this.doctorRepository.save(doctor);
        return this.doctorMapper.fromEntityToDoctorDTO(doctor);
    }

    public void addMedication(MedicationDTO medicationDTO) {
        this.medicationService.addMedication(medicationDTO);
    }

    public void deleteMedication(MedicationDTO medicationDTO) {
        this.medicationService.addMedication(medicationDTO);
    }

    public List<MedicationDTO> getAllMedications() {
        return this.medicationService.findAll();
    }


    public DoctorDTO findByUser(UserDTO user) {
        return doctorMapper.fromEntityToDoctorDTO(doctorRepository.findByUser(userMapper.fromUserDTOtoUserEntity(user)));
    }

    public List<CaregiverDTO> findMyCaregivers(Integer id) {
        Doctor doctor = this.doctorRepository.findById(id).get();
        List<Caregiver> caregivers = doctor.getCaregivers();
        return this.caregiverMapper.fromEntityListToCaregiverDTOList(caregivers);

    }
}
