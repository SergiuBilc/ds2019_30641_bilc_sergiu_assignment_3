package ds.firstassignment.services;

import ds.firstassignment.dto.MonitoredIntakeDTO;
import ds.firstassignment.entities.MonitoredIntake;
import ds.firstassignment.dto.mapper.MonitoredIntakeMapper;
import ds.firstassignment.repositories.MonitoredIntakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MonitoredIntakeService {

    @Autowired
    MonitoredIntakeRepository monitoredIntakeRepository;

    @Autowired
    MonitoredIntakeMapper monitoredIntakeMapper;

    public void addMonitoredIntake(MonitoredIntake monitoredIntake) {

        this.monitoredIntakeRepository.save(monitoredIntake);
    }
    public List<MonitoredIntakeDTO> getAll(){
        return monitoredIntakeMapper.entityListToDtoList( monitoredIntakeRepository.findAll());
    }
}
