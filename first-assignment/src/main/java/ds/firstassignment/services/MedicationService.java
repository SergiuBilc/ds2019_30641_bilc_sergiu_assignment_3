package ds.firstassignment.services;


import ds.firstassignment.dto.IntakeDTO;
import ds.firstassignment.dto.MedicationDTO;
import ds.firstassignment.dto.mapper.IntakeMapper;
import ds.firstassignment.dto.mapper.MedicationMapper;
import ds.firstassignment.entities.Intake;
import ds.firstassignment.entities.Medication;
import ds.firstassignment.entities.Medication;
import ds.firstassignment.repositories.MedicationRepository;
import ds.firstassignment.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicationService {

    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private MedicationMapper medicationMapper;

    @Autowired
    private IntakeMapper intakeMapper;

    @Autowired
    private IntakeService intakeService;

    public void addMedication(MedicationDTO medicationDTO) {
        Medication medication = this.medicationMapper.medicationDTOtoMedication(medicationDTO);
        this.medicationRepository.save(medication);
    }

    public MedicationDTO updateMedication(MedicationDTO medicationDTO) {
        Medication medication = this.medicationMapper.medicationDTOtoMedication(medicationDTO);

        return this.medicationMapper.medicationToMedicationDTO(this.medicationRepository.save(medication));
    }

    public void deleteMedication(Integer medicationID) {

        Medication medication = this.medicationRepository.findById(medicationID).get();
        IntakeDTO intakeDTO = intakeService.findIntakeByMedication(this.medicationMapper.medicationToMedicationDTO(medication));
        this.intakeService.deleteIntake(intakeDTO);
        this.medicationRepository.delete(medication);
    }

    public MedicationDTO findById(Integer id) {
        return this.medicationMapper.medicationToMedicationDTO(this.medicationRepository.findById(id).get());
    }

    public List<MedicationDTO> findAll() {
        return this.medicationMapper.medicationListtoMedicationDTOList(this.medicationRepository.findAll());
    }


}
