package ds.firstassignment.services;

import java.util.List;
import ds.firstassignment.entities.Activity;
import ds.firstassignment.entities.Patient;
import ds.firstassignment.repositories.ActivityRepository;
import ds.firstassignment.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private PatientRepository patientRepository;

    public void addActivity(Activity activity){
        activityRepository.save(activity);
    }

    public List<Activity> getActivitiesForPatient(Integer id){
        Patient patient = patientRepository.getOne(id);

        return this.activityRepository.getActivitiesByPatient(patient);
    }
}
