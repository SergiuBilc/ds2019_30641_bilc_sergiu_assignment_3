package ds.firstassignment.services;

import ds.firstassignment.dto.IntakeDTO;
import ds.firstassignment.dto.MedicationDTO;
import ds.firstassignment.dto.PatientDTO;
import ds.firstassignment.dto.mapper.IntakeMapper;
import ds.firstassignment.dto.mapper.MedicationMapper;
import ds.firstassignment.dto.mapper.PatientMapper;
import ds.firstassignment.entities.Intake;
import ds.firstassignment.entities.Patient;
import ds.firstassignment.repositories.IntakeRepository;
import ds.firstassignment.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IntakeService {

    @Autowired
    private IntakeRepository intakeRepository;

    @Autowired
    private IntakeMapper intakeMapper;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private MedicationMapper medicationMapper;

    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private PatientService patientService;

    public void addIntake(IntakeDTO intakeDTO) {
        Intake intake = this.intakeMapper.intakeDTOtoIntake(intakeDTO);
        this.intakeRepository.save(intake);
    }

    public IntakeDTO updateIntake(IntakeDTO intakeDTO) {
        Intake intake = this.intakeMapper.intakeDTOtoIntake(intakeDTO);
        IntakeDTO updatedIntake = this.intakeMapper.intakeToIntakeDTO(this.intakeRepository.save(intake));

        return updatedIntake;
    }

    public void deleteIntake(IntakeDTO intakeDTO) {
        Intake intake = this.intakeMapper.intakeDTOtoIntake(intakeDTO);
        this.intakeRepository.delete(intake);
    }

    public IntakeDTO findById(Integer id) {
        return this.intakeMapper.intakeToIntakeDTO(intakeRepository.findById(id).get());
    }

    public Intake findEntityById(Integer id) {
        return intakeRepository.findById(id).get();
    }

    public List<IntakeDTO> findAll() {
        return this.intakeMapper.intakeListtoIntakeDTOList(this.intakeRepository.findAll());
    }

    public List<IntakeDTO> findIntakesByPatient(PatientDTO patientDTO) {
        Patient patient = this.patientMapper.patientDTOtoPatient(patientDTO);
        return this.intakeMapper.intakeListtoIntakeDTOList(this.intakeRepository.findByPatient(patient));
    }

    public IntakeDTO findIntakeByMedication(MedicationDTO medicationDTO){
        return this.intakeMapper.intakeToIntakeDTO(this.medicationRepository.findById(medicationDTO.getId()).get().getIntake());
    }

    public List<IntakeDTO> findIntakesByPatientId(int patientId) {
        PatientDTO patientDTO = patientService.findById(patientId);
        return findIntakesByPatient(patientDTO);
    }
}
