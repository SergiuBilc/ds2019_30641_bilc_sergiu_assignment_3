package ds.firstassignment.services;

import ds.firstassignment.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private UserService userService;

    public UserDTO login (String username, String password) throws Exception {

        UserDTO temp = userService.findByUsername(username);
        if(temp.getPassword().equals(password)){
            return temp;
        }else{
            throw new Exception("Incorrect password!");
        }
    }
}
